﻿using System;

namespace Exercise8
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var a = 0;
            Console.WriteLine("What is a?");
            a = int.Parse(Console.ReadLine());

            var b = 0;
            Console.WriteLine("What is b?");
            b = int.Parse(Console.ReadLine());

            var c = 0;
            Console.WriteLine("What is c?");
            c = int.Parse(Console.ReadLine());

            var d = 0;
            Console.WriteLine("What is d?");
            d = int.Parse(Console.ReadLine());

            var e = 0;
            Console.WriteLine("What is e?");
            e = int.Parse(Console.ReadLine());

            var answer = (a + b) * c / (d - e);

            Console.WriteLine($" The answer to {a} + {b} * {c} / {d} - {e} is {answer}");

        }
    }
}
